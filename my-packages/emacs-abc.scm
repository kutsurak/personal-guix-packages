(define-module (my-packages emacs-abc)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system emacs)
  #:use-module ((guix licenses) :prefix license:)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (my-packages python-abc))

;; https://github.com/syohex/emacs-helm-ag/archive/0.58.tar.gz

(define-public emacs-helm-ag
  (package
   (name "emacs-helm-ag")
   (version "0.58")
   (source
    (origin
     (method url-fetch)
     (uri (string-append
           "https://github.com/syohex/emacs-helm-ag/archive/" version ".tar.gz"))
     (file-name (string-append name version))
     (sha256
      (base32
       "0rqsyw3ycfb2qa5y581gkmbp3zsfakhnzrf0q6gb1hrg1iziqdqh"))))
   (build-system emacs-build-system)
   (propagated-inputs
    `(("emacs-helm" ,emacs-helm)))
   (home-page "https://github.com/syohex/emacs-helm-ag")
   (synopsis "Find shell history with Emacs Helm")
   (description "This package provides an Emacs Helm interface to search
throw a shell history.")
   (license license:gpl3+)))

(define-public emacs-pipenv
  (let ((commit "5582bf60577de74e6301871c6b77ac86b6ce1970"))
    (package
     (name "emacs-pipenv")
     (version "0.0.1-beta")
     (source
      (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/pwalsh/pipenv.el")
             (commit commit)))
       (file-name (string-append name "-checkout-" commit))
       (sha256
        (base32 "0zbq7g43xlvrp6c95v2c21yqrv54dhfy9vxn010scdc8ljfmi0gr"))))
     (build-system emacs-build-system)
     (inputs
      `(("emacs-f" ,emacs-f)))
     (home-page "https://github.com/pwalsh/pipenv.el")
     (synopsis "A Pipenv porcelain inside Emacs")
     (description "A Pipenv porcelain inside Emacs")
     (license license:gpl3+))))

(define-public emacs-cov
  (let ((commit "7c72a949b9628296af97cc7e4df0af6c3824d66e"))
    (package
     (name "emacs-cov")
     (version "0.0.1-beta")
     (source
      (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/AdamNiederer/cov.git")
             (commit commit)))
       (file-name (string-append name "-checkout-" commit))
       (sha256
        (base32 "0rddchwanrshfpjiigmz6a0zz1sz9kxbbgvszvja2r4w0m6irb80"))))
     (build-system emacs-build-system)
     (inputs
      `(("emacs-f" ,emacs-f)))
     (propagated-inputs
      `(("emacs-elquery" ,emacs-elquery)))
     (home-page "https://github.com/AdamNiederer/cov")
     (synopsis "An emacs extension for displaying coverage data on your code")
     (description "cov shows code coverage data for your program in emacs. Currently, it supports gcov, lcov, and clover output, as well as the Coveralls format produced by undercover.el.")
     (license license:gpl3+))))

(define-public emacs-elquery
  (let ((commit "eac429d8550fbf1582c57d5e16fed9f320d6eb30"))
    (package
     (name "emacs-elquery")
     (version "0.0.1-beta")
     (source
      (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/AdamNiederer/elquery.git")
             (commit commit)))
       (file-name (string-append name "-checkout-" commit))
       (sha256
        (base32 "1jkbrv5r5vzqjhadb4dcgks47gaj7aavzdkzc5gjn5zv5fmm1in2"))))
     (build-system emacs-build-system)
     (inputs
      `(("emacs-s" ,emacs-s)
        ("emacs-dash" ,emacs-dash)))
     (home-page "https://github.com/AdamNiederer/elquery")
     (synopsis "Read and manipulate HTML in emacs")
     (description " Read and manipulate HTML in emacs ")
     (license license:gpl3+))))

(define-public emacs-dired-rainbow
  (let ((commit "9b2a89951cee8bdf5c0cb67f9c3ad6ac73abf9cb"))
    (package
     (name "emacs-dired-rainbow")
     (version "0.0.1-beta")
     (source
      (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/purcell/diredfl/")
             (commit commit)))
       (file-name (string-append name "-checkout-" commit))
       (sha256
        (base32 "0x4qhxysmcwllkbia6xkfmlpddxhfxxvawywp57zs8c00193nn1z"))))
     (build-system emacs-build-system)
     (home-page "https://github.com/purcell/diredfl/")
     (synopsis "Add colors to dired")
     (description "Repackage the color stuff from dired+ in a git friendly way")
     (license license:gpl3+))))
;; (define-public emacs-pycoverage
;;   (let ((commit "4f5451f4d6e1e2ddd5878fc7d18f5fc4fc92a83d"))
;;     (package
;;      (name "emacs-pycoverage")
;;      (version "0.0.1-beta")
;;      (source
;;       (origin
;;        (method git-fetch)
;;        (uri (git-reference
;;              (url "https://github.com/mattharrison/pycoverage.el.git")
;;              (commit commit)))
;;        (file-name (string-append name "-checkout-" commit))
;;        (sha256
;;         (base32 "0xhkzskxnj4lmf4152c3n1vp68l1xgwa277f3yg8sslg6ixkd2p0"))))
;;      (outputs '("out" "cov2emacs"))
;;      (build-system emacs-build-system)
;;      (propagates-inputs
;;       `(("python-cov2emacs" ,python-cov2emacs)))
;;      ;; (inputs
;;      ;;  `(("emacs-s" ,emacs-s)
;;      ;;    ("emacs-dash" ,emacs-dash)))
;;      (home-page "https://github.com/mattharrison/pycoverage.el.git")
;;      (synopsis "emacs support for highighting python coverage results")
;;      (description "An emacs minor mode for reporting inline on coverage stats for Python")
;;      (license license:gpl3+))))
