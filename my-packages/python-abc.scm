(define-module (my-packages python-abc)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system python)
  #:use-module ((guix licenses) :prefix license:)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages check)
  #:use-module (gnu packages python))

;; The upstream code is broken... :-/
;; (define-public python-cov2emacs
;;   (let ((commit "4f5451f4d6e1e2ddd5878fc7d18f5fc4fc92a83d"))
;;     (package
;;      (name "python-cov2emacs")
;;      (version "0.0.1-beta")
;;      (source
;;       (origin
;;        (method git-fetch)
;;        (uri (git-reference
;;              (url "https://github.com/mattharrison/pycoverage.el.git")
;;              (commit commit)))
;;        (file-name (string-append name "-checkout-" commit))
;;        (sha256
;;         (base32 "0xhkzskxnj4lmf4152c3n1vp68l1xgwa277f3yg8sslg6ixkd2p0"))))
;;      (arguments
;;       `(#:python ,python-2
;;         #:phases
;;           (modify-phases %standard-phases
;;                          (add-after 'unpack 'fix-path
;;                                     (lambda* (#:key inputs #:allow-other-keys)
;;                                       (chdir (string-append (getcwd) "/cov2emacs")))))))
;;      (build-system python-build-system)
;;      (inputs
;;       `(("python-coverage" ,python-coverage)))
;;      ;; (inputs
;;      ;;  `(("emacs-s" ,emacs-s)
;;      ;;    ("emacs-dash" ,emacs-dash)))
;;      (home-page "https://github.com/mattharrison/pycoverage.el.git")
;;      (synopsis "emacs support for highighting python coverage results")
;;      (description "An emacs minor mode for reporting inline on coverage stats for Python")
;;      (license license:gpl3+))))
